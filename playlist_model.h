#ifndef SOUNDWAVE_PLAYLIST_MODEL_H
#define SOUNDWAVE_PLAYLIST_MODEL_H

#include <QAbstractItemModel>
#include <QUrl>

struct PlaylistItem {
	QString name;
	QUrl location;
};

class PlaylistModel : public QAbstractItemModel
{
	Q_OBJECT
	
public:
	PlaylistModel( QObject *parent = 0 );

	virtual QVariant data( const QModelIndex & index, int role = Qt::DisplayRole ) const;
	virtual QModelIndex index( int row, int col, const QModelIndex & parent = QModelIndex()) const;

	virtual QModelIndex parent( const QModelIndex & index ) const;
	virtual int rowCount( const QModelIndex & parent = QModelIndex()) const;
	virtual int colCount( const QModelIndex & parent = QModelIndex()) const;

private:
	QList<PlaylistItem> items;
};

#endif
