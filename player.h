#ifndef SOUNDWAVE_PLAYER_H
#define SOUNDWAVE_PLAYER_H

#include <QProcess>
#include <QTemporaryFile>
#include <QThread>
#include <QUrl>

class Player : public QThread
{
public:
	Player( QUrl url );
	Player( QByteArray song );

	// tells the thread to exit
	void stop();

protected:
	virtual void run();

private:
	int stop_flag;
	QString arg;

	QProcess *p;
	QTemporaryFile file;
};

#endif // SOUNDWAVE_PLAYER_H
