#ifndef SOUNDWAVE_PLAYLIST_H
#define SOUNDWAVE_PLAYLIST_H

#include <QDialog>
#include <QNetworkAccessManager>
#include <QStringListModel>

#include "player.h"
#include "ui_playlist.h"

class Playlist : public QDialog
{
	Q_OBJECT

public slots:
	void on_addItems_clicked();
	void on_removeItems_clicked();
	void on_startPlay_clicked();
	void on_stopPlay_clicked();

	void readNetwork( QNetworkReply * );

public:
	Playlist();
	~Playlist();

protected:
	void addM3U( const QString & data );

private:
	QStringListModel *list;

	QNetworkAccessManager *manager;
	Ui::Playlist *ui;

	Player *player;
};


#endif // SOUNDWAVE_PLAYLIST_H
