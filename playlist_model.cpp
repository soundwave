#include "playlist_model.h"

//=========================================================
PlaylistModel::PlaylistModel( QObject *parent )
	: QAbstractItemModel( parent )
{

}
//=========================================================
QVariant PlaylistModel::data( const QModelIndex &index, int role ) const
{
	// invalid index position, so we return nothing
	if( !index.isValid() || index.column() > 0 || index.row() >= items.size())
		return QVariant();

	// return the title of the song for the display role
	switch( role ){
	case Qt::DisplayRole:
		return items[ index.row()].name;
	}

	// for unhandled roles, we just return NULL data
	return QVariant();
}
//=========================================================
QModelIndex PlaylistModel::index( int row, int col, const QModelIndex &parent ) const
{
	// return invalid index for invalid input parameters
	if( col != 0 || row < 0 || parent.isValid())
		return QModelIndex();

	// row cannot be larger then size of the playlist
	if( row >= items.size())
		return QModelIndex();

	// ok, this is a valid row,col so we return a valid index for it
	return createIndex( row, col );
}
//=========================================================
QModelIndex PlaylistModel::parent( const QModelIndex &index ) const
{
	// No parents for any index - flat list
	return QModelIndex();
}
//=========================================================
int PlaylistModel::rowCount( const QModelIndex & parent) const
{
	if (parent.isValid())
		return 0;
	else
		// Number of Rows is equal to the number of items in the list
		return items.size();
}
//=========================================================
int PlaylistModel::colCount( const QModelIndex & parent) const
{

	if (parent.isValid())
		return 0;
	else
		// PlayList always has one column
		return 1;
}
