#include <QFile>
#include <QInputDialog>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QUrl>
#include "playlist_view.h"

//=========================================================
Playlist::Playlist() : QDialog( 0 )
{
	ui = new Ui::Playlist;
	ui->setupUi( this );

	manager = new QNetworkAccessManager( this );
	connect( manager, SIGNAL(finished(QNetworkReply*)), this, SLOT(readNetwork(QNetworkReply*)));

	list = new QStringListModel( this );
	ui->playlistView->setModel( list );

	player = 0;
}
//=========================================================
Playlist::~Playlist()
{
	delete ui;
	delete manager;
}
//=========================================================
void Playlist::on_addItems_clicked()
{
	QUrl url( QInputDialog::getText( this, "Playlist URL", "URL" ));
	if( !url.isValid())
		return;

	qDebug( "Opening %s", qPrintable( url.toString()));
	if( url.scheme() == "http" )
		manager->get( QNetworkRequest( url ));
	else if( url.scheme() == "file" ){
		QFile f( url.toLocalFile());
		if( !f.open( QFile::ReadOnly )){
			qDebug( "can't open file: %s", qPrintable( f.fileName()));
			return;
		}

		addM3U( f.readAll());
	}
	else
		qDebug( "No handlers for this scheme" );
}
//=========================================================
void Playlist::on_removeItems_clicked()
{
	qDebug( "FIXME" );
}
//=========================================================
void Playlist::on_startPlay_clicked()
{
	if( player ){
		// stop current play
		player->stop();
		delete player;
		player = 0;
	}

	QStringList items = list->stringList();
	if( items.isEmpty()){
		qDebug( "empty list" );
		return;
	}

	manager->get( QNetworkRequest( QUrl( items.first())));
}
//=========================================================
void Playlist::on_stopPlay_clicked()
{
	if( player ){
		player->stop();
		delete player;
		player = 0;
	}
}
//=========================================================
void Playlist::addM3U( const QString & data )
{
	QStringList m3u_list = data.split( "\n" );
	if( m3u_list.isEmpty())
		return;

	if( m3u_list.takeFirst().simplified() != "#EXTM3U" ){
		qDebug( "Invalid M3U signature" );
		return;
	}

	QStringList dataList = list->stringList();
	while( !m3u_list.isEmpty()){
		QString item = m3u_list.takeFirst().simplified();
		if( item.startsWith('#'))
			continue;

		// add URL to list
		QUrl url( item );
		if( !url.isValid())
			continue;

		dataList.append( item );
	}

	list->setStringList( dataList );
}
//=========================================================
void Playlist::readNetwork( QNetworkReply *reply )
{
	qDebug( "__FUNCTION__ " );

	if( reply->error() == QNetworkReply::NoError ){
		QByteArray data = reply->readAll();
		if( data.left( 7 ) == QByteArray( "#EXTM3U"))
			addM3U( data );
		else if (!player){
			player = new Player( data );
			player->start( QThread::LowestPriority );
		} else {
			qDebug("Player already exists don't create a new one");	
		}
	}
	else
		qDebug( "fetch failed: %d", (int)reply->error());
}
