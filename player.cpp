#include "player.h"
#include <QProcess>
#include <QProcessEnvironment>
#include <QTemporaryFile>

//=========================================================
Player::Player(QUrl url)
{
	arg = url.toString();
	stop_flag = 0;
}
//=========================================================
Player::Player(QByteArray song)
{
	file.open();
	file.write( song );

	arg = file.fileName();
	stop_flag = 0;
}

//=========================================================
void Player::stop()
{
	stop_flag = 1;
	wait( -1 );
}
//=========================================================
void Player::run()
{
	p = new QProcess();
	QProcessEnvironment env = p->processEnvironment();
	if( env.contains( "http_proxy" ))
		env.remove( "http_proxy" );
	p->setProcessEnvironment( env );
	p->start( "mplayer", QStringList() << arg );
	p->waitForStarted( -1 );

	while( !stop_flag ){
		while( p->canReadLine())
			qDebug( "mplayer: %s", p->readLine().constData());

		if( p->waitForFinished( 250 ))
			break;
	}
	
	p->terminate();
	delete p;
}
